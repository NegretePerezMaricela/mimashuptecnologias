<?php
session_start();

if(!isset($_SESSION['access_token'])) {
  header('Location: google-login.php');
  exit(); 
}

?>

<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.1.9/jquery.datetimepicker.min.css" />
  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.1.9/jquery.datetimepicker.min.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

  <style>
     #map {
        height: 400px;
        width: 100%;
      }
  </style>
  <style>
     #search-container {
       height: 300px;
       width: 1500px;
       border: 1px solid gray;
      }
 </style>
 <style>
    .pagination {list-style:none; margin:0px; padding:0px;}
    .pagination li{float:left; margin:3px;}
    .pagination li a{   display:block; padding:3px 5px; color:#fff; background-color:#44b0dd; text-decoration:none;}
    .pagination li a.active {border:1px solid #000; color:#000; background-color:#fff;}
    .pagination li a.inactive {background-color:#eee; color:#777; border:1px solid #ccc;}
  </style>

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
  <script src="auth.js"></script>
  <script src="search.js"></script>
  <script src="https://apis.google.com/js/client.js?onload=googleApiClientReady"></script>
  <script async defer
   src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7ve9_mDBQRkYnHghwsnsRvybuPpOFpcQ&callback=initMap">
  </script>

  <style>
  div.container {
    width: 100%;
    border: 1px solid gray;
  }

  header, footer {
    padding: 1em;
    color: white;
    background-color: black;
    clear: left;
    text-align: center;
  }

  nav {
    float: left;
    max-width: 460px;
    margin: 1%;
    padding: 1em;
  }

  nav ul {
    list-style-type: none;
    padding: 0;
  }
   
  nav ul a {
    text-decoration: none;
  } 

  article {
    margin-left: 50px;
    border-left: 1px solid gray;
    padding: 1em;
    overflow: hidden;
  }
  .button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 16px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.4s;
    cursor: pointer;
  }
  .button1 {
    background-color: white; 
    color: black; 
    border: 2px solid #4CAF50;
  }

  .button1:hover {
    background-color: #4CAF50;
    color: white;
  }

 .button5 {
    background-color: white;
    color: black;
    border: 2px solid #555555;
  }

  .button5:hover {
    background-color: #555555;
    color: white;
  }

  </style>
</head>

<body>

  <div class="container">
  <header>
   <h1>Mashup (Maps-YouTube-Calendar)</h1>
  </header>
  
  <nav>
    <div class="w3-container w3-teal" >
      <h3>Crear Evento</h3>
    </div>
    <div id="form-container">  
    <form  class="w3-container">
      <input class="w3-input w3-border w3-light-grey" type="text" id="event-title" placeholder="Titulo del Evento" autocomplete="off" /> 
      <br>
      <select class="w3-input w3-border w3-light-grey" id="event-type"  autocomplete="off">
        <option value="FIXED-TIME">Fixed Time Event</option>
        <option value="ALL-DAY">All Day Event</option>
      </select>
      <br>
      <input class="w3-input w3-border w3-light-grey" type="text" id="event-start-time" placeholder="Inicio del Evento" autocomplete="off" />
      <br>
      <input class="w3-input w3-border w3-light-grey" type="text" id="event-end-time" placeholder="Fin del Evento" autocomplete="off" />
      <br>
      <input class="w3-input w3-border w3-light-grey" type="text" id="event-date" placeholder="Fecha del Evento" autocomplete="off" />
      <br>
      <input class="w3-input w3-border w3-light-grey" type="text" id="location" placeholder="Localizacion del Evento" autocomplete="off" />
      <br>
      <button class="button button1" id="create-event">Crear Evento</button>
      <button  class="button button1" onclick="limpiar()" id ="borrar">Limpiar</button>
    </form>  
    </div>
  </nav>

  <article>
    <div id="map"></div> 
    <label class="w3-text-brown" > 
      Cantidad de Videos:
      <input class="w3-input w3-border w3-sand" id="maxres" value='30' type="text"/>   <br>
      Buscador:
      <input class="w3-input w3-border w3-sand" id="query" value='Documental Bipolaridad' type="text"/>   <br>
      <button class="w3-btn w3-brown" id="search-button" disabled onclick="search(),limpiar()">Search</button>
    </label>
  </article>


  <footer> Resultado de Busquedas </footer>
  <div id="search-container">  </div>
</div>

  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.1.9/jquery.datetimepicker.min.js"></script>

  <script>

  function limpiar() {
      document.getElementById("event-title").value = ""; 
      document.getElementById("event-start-time").value = ""; 
      document.getElementById("event-end-time").value = ""; 
      document.getElementById("event-date").value = "";
      document.getElementById("location").value = "";
    }
  function AdjustMinTime(ct) {
      var dtob = new Date(),
      current_date = dtob.getDate(),
      current_month = dtob.getMonth() + 1,
      current_year = dtob.getFullYear();
        
      var full_date = current_year + '-' +
          ( current_month < 10 ? '0' + current_month : current_month ) + '-' + 
            ( current_date < 10 ? '0' + current_date : current_date );

      if(ct.dateFormat('Y-m-d') == full_date)
        this.setOptions({ minTime: 0 });
      else 
        this.setOptions({ minTime: false });
  }

  $("#event-start-time, #event-end-time").datetimepicker({ format: 'Y-m-d H:i', minDate: 0, minTime: 0, step: 5, onShow: AdjustMinTime, onSelectDate: AdjustMinTime });
  $("#event-date").datetimepicker({ format: 'Y-m-d', timepicker: false, minDate: 0 });

  $("#event-type").on('change', function(e) {
    if($(this).val() == 'ALL-DAY') {
    $("#event-date").show();
    $("#event-start-time, #event-end-time").hide();
  }
  else {
    $("#event-date").hide(); 
    $("#event-start-time, #event-end-time").show();
  }
  });

  $("#create-event").on('click', function(e) {
    if($("#create-event").attr('data-in-progress') == 1)
      return;

  var blank_reg_exp = /^([\s]{0,}[^\s]{1,}[\s]{0,}){1,}$/,
    error = 0,
    parameters;

  $(".input-error").removeClass('input-error');

  if(!blank_reg_exp.test($("#event-title").val())) {
    $("#event-title").addClass('input-error');
    error = 1;
  }

    if(!blank_reg_exp.test($("#query").val())) {
    $("#query").addClass('input-error');
    error = 1;
  }

  if(!blank_reg_exp.test($("#location").val())) {
    $("#location").addClass('input-error');
    error = 1;
  }

  if($("#event-type").val() == 'FIXED-TIME') {
    if(!blank_reg_exp.test($("#event-start-time").val())) {
      $("#event-start-time").addClass('input-error');
      error = 1;
    }   

  if(!blank_reg_exp.test($("#event-end-time").val())) {
      $("#event-end-time").addClass('input-error');
      error = 1;
    }
  }
  else if($("#event-type").val() == 'ALL-DAY') {
    if(!blank_reg_exp.test($("#event-date").val())) {
      $("#event-date").addClass('input-error');
      error = 1;
    } 
  }

  if(error == 1)
    return false;

  if($("#event-type").val() == 'FIXED-TIME') {
    // If end time is earlier than start time, then interchange them
    if($("#event-end-time").datetimepicker('getValue') < $("#event-start-time").datetimepicker('getValue')) {
      var temp = $("#event-end-time").val();
      $("#event-end-time").val($("#event-start-time").val());
      $("#event-start-time").val(temp);
    }
  }

  parameters = {  title: $("#event-title").val(), 
          description: $("#query").val(),
          location: $("#location").val(),         
          event_time: {
            start_time: $("#event-type").val() == 'FIXED-TIME' ? $("#event-start-time").val().replace(' ', 'T') + ':00' : null,
            end_time: $("#event-type").val() == 'FIXED-TIME' ? $("#event-end-time").val().replace(' ', 'T') + ':00' : null,
            event_date: $("#event-type").val() == 'ALL-DAY' ? $("#event-date").val() : null
          },
          all_day: $("#event-type").val() == 'ALL-DAY' ? 1 : 0,
          
        }; console.log(parameters);


  $("#create-event").attr('disabled', 'disabled');
  $.ajax({
        type: 'POST',
        url: 'ajax.php',
        data: { event_details: parameters },
        dataType: 'json',
        success: function(response) {
          $("#create-event").removeAttr('disabled');
          alert('EVENTO CREADO CON ID : ' + response.event_id);

        },
        error: function(response) {
            $("#create-event").removeAttr('disabled');
            alert(response.responseJSON.message);
        }
    });
});
</script>

  <script src="http://c.fzilla.com/1286136086-jquery.js"></script>
  <script src="1291523190-jpaginate.js"></script>
</body>
</html>
