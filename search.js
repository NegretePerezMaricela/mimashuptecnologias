var API_KEY = 'AIzaSyB7ve9_mDBQRkYnHghwsnsRvybuPpOFpcQ';
var CLIENT_ID = '882743690267-bj3eb867j9nk5nl1pr7s8ccd9botporp.apps.googleusercontent.com';
var SCOPES = [ 'https://www.googleapis.com/auth/youtube','https://maps.googleapis.com/maps/api/js?key=AIzaSyB7ve9_mDBQRkYnHghwsnsRvybuPpOFpcQ&callback=initMap'];

function tplawesome(e,t){res=e;for(var n=0;n<t.length;n++){res=res.replace(/\{\{(.*?)\}\}/g,function(e,r){return t[n][r]})}return res}
function handleAPILoaded() {
  $('#search-button').attr('disabled', false);
}

function initClient(){
gapi.client.init({
apiKey:API_KEY,
clientID: CLIENT_ID,
scope: SCOPES});
}

var videoIDStringFinal= '';
var maximo = '';
var q='';

function search() {


var iframes = [];
var ancho = 400 / maximo;
var videoIDString = '';
var resultsArr = [];
maximo = $('#maxres').val();
q = $('#query').val();

var request = gapi.client.youtube.search.list({
    q: q,
    part: 'snippet',
    maxResults: maximo
    });

  request.execute(function(response) {
   
   var str = JSON.stringify(response.result);
   var results = response.result;
         $("#search-container").html(""); 

   /*
          $.each(results.items, function(index, item) {
            $.get("item.html", function(data) {
                $("#search-container").append(tplawesome(data, [{"title":item.snippet.title, "videoid":item.id.videoId}]));
            });

          });*/
   var par= JSON.parse(str);
    for(var i in par.items) {
    var item = par.items[i];
    resultsArr.push(item);
    videoIDString = videoIDString + item.id.videoId + ",";

    videoIDStringFinal = videoIDString.substring(0, videoIDString.length - 1);
    
    var iframe = document.createElement("iframe");
    var url = "//www.youtube.com/embed/"+item.id.videoId;
    iframe.src = url;
    iframes.push(iframe);
    }
  
  var height= 300 / 10;
  var width= 1350 / 10;
  

   for(var i=0; i<iframes.length; i++){
   
   iframes[i].setAttribute("height",300);
   iframes[i].setAttribute("width", width);
          $("#search-container").append(iframes[i]);   
   }
   
   iframes = [];

    videos(videoIDStringFinal);
     $.get("search.php", function(data) {
                $("#search-container").jPaginate(); 

      });


  }); 

}


function videos( ids ){
  initMap();
      var cordenadas = {};
 
      var videoIDRequest = gapi.client.youtube.videos.list({
        id: ids,
        part: 'id,snippet,recordingDetails',
        key: API_KEY
      });

        videoIDRequest.execute(function(response) {
         
          var elementos = JSON.stringify(response.result);
          var par2= JSON.parse(elementos);
                   
              for(var i in par2.items) {
              var items = par2.items[i];

              if(items.recordingDetails && items.recordingDetails.location){
                 var lat = items.recordingDetails.location.latitude;
                  var long = items.recordingDetails.location.longitude;

                 console.log(lat+ ',' + long);
                 cordenadas.lat = lat;
                 cordenadas.lng = long;
                 puntear(cordenadas);

              }
              }
        

      });

      
}

function initMap(){
       var uluru = {lat: 17.0669, lng: -96.7203};
        map = new google.maps.Map(document.getElementById("map"), {
          zoom: 5,
          center: uluru
        });
}


function puntear(coor){
         var marker = new google.maps.Marker({
          position: coor,
          map: map
          
        });

        marker.setMap(map);
}
